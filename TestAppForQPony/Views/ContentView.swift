//
//  ContentView.swift
//  TestAppForQPony
//
//  Created by Anton Medvediev on 09/04/2020.
//  Copyright © 2020 Anton Medvediev. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    init() {
        // To remove only extra separators below the list:
        UITableView.appearance().tableFooterView = UIView()
        
        // To remove all separators including the actual ones:
        UITableView.appearance().separatorStyle = .none
    }
    
    @ObservedObject var apiManager = ApiManager()
    @State var buttonDisabled:Bool = false
    @State var activeTable:String = "A"
    
    var body: some View {
        VStack{
            HStack{
                buttonChangeTable(label: "A")
                buttonChangeTable(label: "B")
                buttonChangeTable(label: "C")
            }.disabled(buttonDisabled)
            if apiManager.observationTable == 0{
                GeometryReader{_ in
                    SpinnerLoader()
                }.onAppear {
                    self.apiManager.fetchDataForTable(self.activeTable)
                }
            }else{
                NavigationView{
                    List(apiManager.cellsTable){ cell in
                        NavigationLink(destination: DetailCellView(currency: cell.currency,code: cell.code, activeTable: self.activeTable)) {
                            VStack{
                                Text(cell.currency)
                                Text("\(cell.code): \(cell.mid ?? (cell.ask!+cell.bid!)/2)")
                                Text(self.apiManager.effectiveDate)
                            }
                            .frame(maxWidth: .infinity, alignment: .center)
                            .padding(EdgeInsets(top: 5, leading: 15, bottom: 5, trailing: 15))
                            .clipShape(Capsule())
                            .overlay(Capsule().stroke(Color.green,lineWidth: 3))
                            
                        }
                    }.onDisappear {
                        self.buttonDisabled = true
                    }.onAppear{
                        self.buttonDisabled = false
                    }
                    .navigationBarTitle("NBP")
                }
            }
        }
    }
    func buttonChangeTable(label:String) -> some View {
        return Button(action: {
            self.activeTable = label
            self.apiManager.fetchDataForTable(self.activeTable)
        }) {
            if self.activeTable == label {
                Text("Table \(label)")
                    .foregroundColor(.black)
                    .padding(10)
                    .background(Color.green)
                    .cornerRadius(30)
            }else{
                Text("Table \(label)")
                    .foregroundColor(.black)
                    .padding(10)
                    .background(Color.gray)
                    .cornerRadius(30)
            }
        }.disabled(buttonDisabled)
    }
    
    
    
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}


