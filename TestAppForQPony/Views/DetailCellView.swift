//
//  DetailCellView.swift
//  TestAppForQPony
//
//  Created by Anton Medvediev on 09/04/2020.
//  Copyright © 2020 Anton Medvediev. All rights reserved.
//

import SwiftUI

struct DetailCellView: View {
    
    @ObservedObject var apiManager = ApiManager()
    
    let currency:String
    let code:String
    let activeTable:String
    
    var dateFormatter: DateFormatter{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }
    
    @State var dateFrom = Date()
    @State var dateTo = Date()
    
    var body: some View {
        VStack{
            Form{
                Section(header: Text("Select Date")) {
                    DatePicker(selection: $dateFrom,in: ...Date(), displayedComponents: .date, label:{
                        Text("Date from")
                    })
                    DatePicker(selection: $dateTo,in: ...Date(), displayedComponents: .date, label:{
                        Text("Date to")
                    })
                }
                Button(action: {
                    self.updateApi()
                }) {
                    Text("Find")
                }
                Section(header: Text("Currency")) {
                    if apiManager.observationDates == 0{
                        GeometryReader{_ in
                            SpinnerLoader()
                        }
                    }else if apiManager.observationDates == 2{
                        Text("Change date to working days and in a range of year and find")
                    }else{
                        List(apiManager.cellsDates){ cell in
                            HStack{
                                Text(cell.effectiveDate)
                                Text("\(cell.mid ?? (cell.ask!+cell.bid!)/2)")
                            }
                        }
                    }
                }
            }
            .navigationBarTitle(currency)
        }
    }
    func updateApi(){
        self.apiManager.fetchDataForDate(self.activeTable, self.code, self.dateFormatter.string(from: self.dateFrom), self.dateFormatter.string(from: self.dateTo))
    }
}

struct DetailCellView_Previews: PreviewProvider {
    static var previews: some View {
        DetailCellView(currency: "String",code:"USD",activeTable: "A")
    }
}
