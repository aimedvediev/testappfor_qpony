//
//  ApiManager.swift
//  TestAppForQPony
//
//  Created by Anton Medvediev on 09/04/2020.
//  Copyright © 2020 Anton Medvediev. All rights reserved.
//

import Foundation

class ApiManager: ObservableObject {
    
    @Published var cellsTable = [CellTables]()
    @Published var effectiveDate = ""
    
    @Published var observationTable: Int = 0
    @Published var observationDates: Int = 2
    
    //observation = 0 loading data from json
    //observation = 1 get data from json
    //observation = 2 no data from json
    
    func fetchDataForTable(_ tableName:String){
        self.observationTable = 0
        if let url = URL(string: "https://api.nbp.pl/api/exchangerates/tables/\(tableName)/"){
            //let session = URLSession(configuration: .default)
            let task = URLSession.shared.dataTask(with: url){
                (data,response,error) in
                if error == nil{
                    let decoder = JSONDecoder()
                    if let safeData = data{
                        do{
                            let result = try decoder.decode([ApiDataTable].self, from: safeData)
                            DispatchQueue.main.async {
                                self.effectiveDate = result[0].effectiveDate
                                self.cellsTable = result[0].rates
                                self.observationTable = 1
                            }
                        }catch{
                            print(error)
                        }
                    }
                }
            }
            task.resume()
        }
    }
    
    @Published var cellsDates = [CellDates]()
    
    func fetchDataForDate(_ tableName:String,_ code:String,_ dateFrom:String,_ dateTo:String){
        self.observationDates = 0
        if let url = URL(string: "https://api.nbp.pl/api/exchangerates/rates/\(tableName)/\(code)/\(dateFrom)/\(dateTo)/"){
            let session = URLSession(configuration: .default)
            let task = session.dataTask(with: url){
                (data,response,error) in
                if error == nil{
                    let decoder = JSONDecoder()
                    if let safeData = data{
                        do{
                            let result = try decoder.decode(ApiDataDates.self, from: safeData)
                            DispatchQueue.main.async {
                                self.cellsDates = result.rates
                                self.observationDates = 1
                            }
                        }catch{
                            DispatchQueue.main.async {
                                self.observationDates = 2
                            }
                            print(error)
                        }
                    }
                }
            }
            task.resume()
        }
    }
}
