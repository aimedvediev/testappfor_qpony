//
//  ApiDataDates.swift
//  TestAppForQPony
//
//  Created by Anton Medvediev on 11/04/2020.
//  Copyright © 2020 Anton Medvediev. All rights reserved.
//

import Foundation

struct ApiDataDates: Decodable {
    let currency:String
    let rates: [CellDates]
    let code:String
}

struct CellDates:Decodable, Identifiable{
    var id :String{
        return no
    }
    let no :String
    let effectiveDate:String
    let bid:Double?
    let ask:Double?
    var mid:Double?
}


//MARK: - Json DatesA
//{
//    "table": "A",
//    "currency": "funt szterling",
//    "code": "GBP",
//    "rates": [
//        {
//            "no": "1/A/NBP/2012",
//            "effectiveDate": "2012-01-02",
//            "mid": 5.3480
//        },
//        {
//            "no": "2/A/NBP/2012",
//            "effectiveDate": "2012-01-03",
//            "mid": 5.3394
//        }
//    ]
//}

//MARK: - Json DatesC
//{
//    "table": "C",
//    "currency": "funt szterling",
//    "code": "GBP",
//    "rates": [
//        {
//            "no": "1/C/NBP/2012",
//            "effectiveDate": "2012-01-02",
//            "bid": 5.2774,
//            "ask": 5.3840
//        },
//        {
//            "no": "2/C/NBP/2012",
//            "effectiveDate": "2012-01-03",
//            "bid": 5.3004,
//            "ask": 5.4074
//        }
//    ]
//}
