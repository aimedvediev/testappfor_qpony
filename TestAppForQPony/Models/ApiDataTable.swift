//
//  ApiData.swift
//  TestAppForQPony
//
//  Created by Anton Medvediev on 09/04/2020.
//  Copyright © 2020 Anton Medvediev. All rights reserved.
//

import Foundation

struct ApiDataTable: Decodable {
    let effectiveDate:String
    let rates: [CellTables]
}

struct CellTables:Decodable, Identifiable{
    var id :String{
        return code
    }
    let currency:String
    let code:String
    let bid:Double?
    let ask:Double?
    var mid:Double?
}




//#MARK: - Json TableA
//[
//    {
//        "table": "A",
//        "no": "071/A/NBP/2020",
//        "effectiveDate": "2020-04-10",
//        "rates": [
//            {
//                "currency": "bat (Tajlandia)",
//                "code": "THB",
//                "mid": 0.1272
//            },
//            {
//                "currency": "dolar amerykański",
//                "code": "USD",
//                "mid": 4.1566
//            }
//        ]
//    }
//]
//#MARK: - Json TableC
//[
//    {
//        "table": "C",
//        "no": "071/C/NBP/2020",
//        "tradingDate": "2020-04-09",
//        "effectiveDate": "2020-04-10",
//        "rates": [
//            {
//                "currency": "dolar amerykański",
//                "code": "USD",
//                "bid": 4.1117,
//                "ask": 4.1947
//            },
//            {
//                "currency": "dolar australijski",
//                "code": "AUD",
//                "bid": 2.5907,
//                "ask": 2.6431
//            }
//        ]
//    }
//]
